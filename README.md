# hypatos-project

## Introduction 

The purpose of this document is to describe implementation for a simple email-sending service.

## Requirements

Create a service that accepts the necessary information and sends emails. It
should provide an abstraction between two of the following email service providers.
Implement failover: if one of the services goes down, your service can quickly
failover to a different provider without affecting your customers.

Example Email Providers:
- SendGrid
- Mailgun
- Mandrill
- Amazon SES

## Chosen technology stack

The service was implemented using the following technologies:

- [Python](https://www.python.org/) - as a language of choice, Python was selected due to it's expresiveness, popularity and because it's language I feel most comfortable with. To be fair, programming language choice is not crucial for this particular task
- [FastAPI](https://fastapi.tiangolo.com/)  - Web framework written in Python, chosen due to it's simplicity and ability to quickly create web apis and support for auto-generated OpenAPI specs
- [Pydantic](https://pydantic-docs.helpmanual.io/)  - Library for data validation and settings management, chosen due to simplicity and easy integration with FastAPI
- [Redis](https://redis.io/) - used as a caching layer for storing information about unavailability of primary email provider
- [Sentry](https://sentry.io/) - used as an error monitoring tool
- [Docker](https://www.docker.com/) - application is packaged, distributed and run in form of Docker containers, which allows it to be easily deployable with different cloud providers/environments


In addition, the following email providers were chosen:
- [Sendgrid](https://sendgrid.com/)
- [Mailgun](www.mailgun.com)


## High level description of functionality

The main functionality of the service is to provide the ability to send emails via JSON API, while providing failover in situations where primary provider is not available.
The application achieves that by first trying to send an email with primary provider and in case of a failure, it marks the primary provider as "unavailable" and tries to send an email with secondary provider.
The reason for marking it as "unavailable" is to reduce number of requests made to a malfunctioning provider, as the errors will be most likely related to downtime/rate limit on the provider side which usually takes more time. 
The application uses API KEY-based authentication, in current form it's just a dummy, hardcoded key that should be replaced by a "real" auth backend for production deployment.

### How to use it/test it

Frontend of the application consist of an api endpoint `/api/v1/send_email` and interactive documentation, available under `/docs`. 

1. Go to `https://pgrzesik-email-app.herokuapp.com/docs`
![Docs](img/docs.png)
2. Click on "Authorize" and enter `12345` as the key
![Authorize](img/auth.png)
3. Click on "POST /api/v1/send_email"
4. Click on "Try it out", adjust payload so there are no duplicates between `to_emails`, `cc_emails` and `bcc_emails`.
![Send](img/before_send.png)
5. Click on "Execute"
![Response](img/response.png)

Note: First test on Heroku might be a bit slower due to the fact that Heroku shuts down free instances when they're idle for longer than 30 minutes.

## Local development

Prerequisites:
1. Docker installed
2. docker-compose instaled
3. `.env` file with the following configuration

```
MAILGUN_API_KEY=<your-mailgun-api-key>
MAILGUN_API_BASE_URL=<your-mailgun-base-url>
SENDGRID_API_KEY=<your-sendgrid-api-key>
SENTRY_DSN=<optional-your-sentry-dsn>
```

To run the application, use the following command:
`docker-compose up app`

To run the application in background, use the following command:
```
docker-compose up -d app
```

The application will be automatically reloaded after changes

To run tests, use the following command:
```
docker-compose run app pytest
```

### Linting, formatting, type checks

To format code with `black`, use the following command:
```
docker-compose run app black .
```

To lint code with `flake8`, use the following command:
```
docker-compose run app flake8 .
```

To check types with `mypy`, use the following command:
```
docker-compose run app mypy .
```

For all of the above, it is recommended to configure your editor to automatically run these checks vs running them manually 

## Deployment

Currently, the application is deployed on [Heroku](heroku.com) and available under , however, with prepared Dockerfile, the application can be ran anywhere Docker is available.
The reason for choosing Heroku was it's simplicity and relatively quick setup, which makes it perfect for such tasks.

If you would like to deploy it somewhere else, you can build Docker image with the following command:
```
docker build -t email-app:<tag> .
```

While running it later, you need to supply the following environment variables:
```
MAILGUN_API_KEY=<your-mailgun-api-key>
MAILGUN_API_BASE_URL=<your-mailgun-base-url>
SENDGRID_API_KEY=<your-sendgrid-api-key>
SENTRY_DSN=<optional-your-sentry-dsn>
```

## Tradeoffs, missing pieces, potential alternative choices

### Authentication & authorization

In current version, I went with a mocked version of auth, as I didn't want to add extra database layer + logic for managing api keys.
In order to deploy it in production environment, "real" authentication backend would be required.

### Background processing with Celery/rq/huey

In this task, I focused on the solution that serves as a "facade" that provides synchronous access to email providers with failover, where clients expect "instant" response if the request succeeded or failed.
For this reason, I didn't implement background sending of emails with retries, queueing, etc, as it would require extra callbacks with confirmations and so on, which requires two-way communication between the services. 
However, if needed, the service could be easily adjusted to carry out email sending in asynchronous manner.

### Templates support

A lot of email providers offer support for `templates`, however, they're not consistent when it comes to available functionality which is why I left them out from the current implementation. 

### Attachments support

I left out support for attachments to keep it as just a JSON API, support for attachments would require a switch to `multipart/form-data` content type, but it wouldn't be very complicated/time consuming to support it.

### Authorized email addresses/domains

It was not included, however, I believe it would be a good idea to also have a configuration parameter with domains/emails from which the client is allowed to send emails, as the email providers require domain verifications.
It should be fairly easy to add it as a part of config + schema validation.

### FastAPI and async/sync approach

I decided to go with synchronous approach here, as I wanted to take advantage of official Sendgrid SDK for Python which is not asynchronous, however, for this particular use case it would be beneficial 
to consider fully async approach which could improve throughput of the service as the application is IO bound.

### NameEmail scheme explanation

In current implementation, the API expects only emails and does not offer support for e.g. `John Doe <j.doe@email.com>` scheme. It would be fairly easy to adjust it in schema to support it, but I felt like it's not really showing any "extra" skills, so I skipped that part of implementation

### Multiple providers support

In current version, application supports a primary and a secondary provider, however, it would be possible to extend it to more providers which can serve as additional failover option in case if both of the providers are unavailable.

### Scalability

In current form, application is fairly easy to scale as it only has Redis as a single dependency.

### Base docker image

In current version, I'm using `tiangolo/uvicorn-gunicorn-fastapi:python3.7` as base image due to the fact that it has sensible defaults/configuration for uvicorn/gunicorn. For production deployment I wouldn't rely on such
3rd party base image, but I believe it's a reasonable choice for now. Ideally, I would like to use a barebone `python:3.7` as base image.

