import logging
from os import path

import sentry_sdk

from app.api import api_router
from app.config import settings
from fastapi import FastAPI

# Configure logging
log_file_path = path.join(path.dirname(path.abspath(__file__)), "logging.conf")
logging.config.fileConfig(log_file_path, disable_existing_loggers=False)

# Configure Sentry
if settings.SENTRY_DSN:
    sentry_sdk.init(settings.SENTRY_DSN)


app = FastAPI()

app.include_router(api_router, prefix="/api/v1")
