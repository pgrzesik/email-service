from fastapi import Depends, HTTPException
from fastapi.security import APIKeyHeader
from starlette import status

API_KEY = APIKeyHeader(name="X-API-Key")


def validate_api_key(api_key: str = Depends(API_KEY)):
    # TODO: Build "real" api-key provider
    if api_key == "12345":
        return api_key

    raise HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid API Key provided",
    )
