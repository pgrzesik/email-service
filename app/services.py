import logging

from app.cache import get_redis_client
from app.providers import get_mailgun_email_provider, get_sendgrid_email_provider
from app.schema import EmailSchema

logger = logging.getLogger(__name__)


class EmailService:
    def __init__(self, primary_provider, secondary_provider, cache_client):
        self.primary = primary_provider
        self.secondary = secondary_provider
        self.cache_client = cache_client

    def send_email(self, email_params: EmailSchema) -> bool:
        can_send_with_primary = self.is_provider_available(self.primary)
        email_sent_with_primary = False

        if can_send_with_primary:
            logger.info("Sending email with primary provider")
            email_sent_with_primary = self.primary.send_email(email_params)
            if not email_sent_with_primary:
                logger.info(
                    "Could not send email with primary, marking primary provider as unavailable"
                )
                self.mark_provider_as_unavailable(self.primary)
        else:
            logger.info("Primary provider unavailable")

        # Not checking for availability of secondary as it serves as fallback only
        # if it's marked as unavailable we would fail anyway

        if not email_sent_with_primary:
            logger.info("Sending email with secondary provider")
            email_sent_with_secondary = self.secondary.send_email(email_params)

        return email_sent_with_primary or email_sent_with_secondary

    def is_provider_available(self, provider):
        key = self._get_provider_unavailable_key(provider)

        result = self.cache_client.get(key)

        return False if result else True

    def mark_provider_as_unavailable(self, provider):
        key = self._get_provider_unavailable_key(provider)
        timeout = 60  # 1 minute, could be moved to configuration/env variable

        self.cache_client.set(key, "True", timeout)

    def _get_provider_unavailable_key(self, provider):
        return f"PROVIDER_UNAVAILABLE_{provider.name}"


def get_email_service() -> EmailService:
    """
    In current implementation, we are relying on predefined primary and secondary provider.
    If needed, decision about primary/secondary can be moved to settings so it's easier to control it
    via env variables without changing code.
    """
    primary_provider = get_sendgrid_email_provider()
    secondary_provider = get_mailgun_email_provider()
    cache_client = get_redis_client()

    return EmailService(primary_provider, secondary_provider, cache_client)
