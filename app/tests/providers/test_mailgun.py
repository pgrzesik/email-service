from urllib.parse import urljoin

import pytest
import responses

from app.providers.mailgun import MailgunEmailProvider
from app.schema import EmailSchema

TEST_API_KEY = "123"
TEST_BASE_URL = "http://base.url.com"


@pytest.fixture
def email_params():
    return EmailSchema(
        from_email="from@email.com",
        to_emails=["to@email.com"],
        cc_emails=["cc@email.com"],
        bcc_emails=["bcc@email.com"],
        subject="Test subject",
        text="Test text",
    )


@pytest.fixture
def mailgun_provider():
    return MailgunEmailProvider(TEST_API_KEY, TEST_BASE_URL)


def test_correctly_constructs_message(mailgun_provider, email_params):
    # When
    result = mailgun_provider.construct_email_message(email_params)

    # Then
    assert result == {
        "from": "from@email.com",
        "to": ["to@email.com"],
        "cc": ["cc@email.com"],
        "bcc": ["bcc@email.com"],
        "subject": "Test subject",
        "text": "Test text",
        "html": None,
    }


@responses.activate
def test_send_email_handles_error(mailgun_provider, email_params):
    # Given
    responses.add(
        method=responses.POST, url=urljoin(TEST_BASE_URL, "/messages"), status=400
    )

    # When
    result = mailgun_provider.send_email(email_params)

    # Then
    assert not result


@responses.activate
def test_send_email_correctly(mailgun_provider, email_params):
    # Given
    responses.add(
        method=responses.POST, url=urljoin(TEST_BASE_URL, "/messages"), status=200
    )

    # When
    result = mailgun_provider.send_email(email_params)

    # Then
    assert result
