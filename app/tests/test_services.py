from unittest import mock

import pytest

from app.schema import EmailSchema
from app.services import EmailService


@pytest.fixture
def email_params():
    return EmailSchema(
        from_email="from@email.com",
        to_emails=["to@email.com"],
        cc_emails=["cc@email.com"],
        bcc_emails=["bcc@email.com"],
        subject="Test subject",
        text="Test text",
    )


def test_with_primary_not_available(email_params):
    # Given
    mocked_cache, mocked_primary, mocked_secondary = (
        mock.Mock(),
        mock.Mock(),
        mock.Mock(),
    )
    mocked_cache.get.return_value = True
    mocked_secondary.send_email.return_value = True
    service = EmailService(mocked_primary, mocked_secondary, mocked_cache)

    # When
    result = service.send_email(email_params)

    # Then
    assert result

    mocked_primary.send_email.assert_not_called()
    mocked_secondary.send_email.assert_called_once_with(email_params)


def test_with_failed_send_with_primary_and_succesful_with_secondary():
    mocked_cache, mocked_primary, mocked_secondary = (
        mock.Mock(),
        mock.Mock(),
        mock.Mock(),
    )
    mocked_cache.get.return_value = False
    mocked_primary.send_email.return_value = False
    mocked_primary.name = "primary"
    mocked_secondary.send_email.return_value = True
    service = EmailService(mocked_primary, mocked_secondary, mocked_cache)

    # When
    result = service.send_email(email_params)

    # Then
    assert result

    mocked_primary.send_email.assert_called_once_with(email_params)
    mocked_secondary.send_email.assert_called_once_with(email_params)
    mocked_cache.set.assert_called_once_with("PROVIDER_UNAVAILABLE_primary", "True", 60)


def test_with_failed_send_with_both_providers():
    mocked_cache, mocked_primary, mocked_secondary = (
        mock.Mock(),
        mock.Mock(),
        mock.Mock(),
    )
    mocked_cache.get.return_value = False
    mocked_primary.send_email.return_value = False
    mocked_secondary.send_email.return_value = False
    service = EmailService(mocked_primary, mocked_secondary, mocked_cache)

    # When
    result = service.send_email(email_params)

    # Then
    assert not result

    mocked_primary.send_email.assert_called_once_with(email_params)
    mocked_secondary.send_email.assert_called_once_with(email_params)


def test_with_successful_send_with_primary():
    mocked_cache, mocked_primary, mocked_secondary = (
        mock.Mock(),
        mock.Mock(),
        mock.Mock(),
    )
    mocked_cache.get.return_value = False
    mocked_primary.send_email.return_value = True
    service = EmailService(mocked_primary, mocked_secondary, mocked_cache)

    # When
    result = service.send_email(email_params)

    # Then
    assert result

    mocked_primary.send_email.assert_called_once_with(email_params)
    mocked_secondary.send_email.assert_not_called()
