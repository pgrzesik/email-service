import pytest

from app.schema import EmailSchema
from pydantic import ValidationError


def test_converts_recipient_emails_to_lowercase():
    # Given
    data = {
        "from_email": "from@email.com",
        "to_emails": ["TO@email.com"],
        "cc_emails": ["CC@eMaIl.com"],
        "bcc_emails": ["bCC@eMaIl.com", "anOtHerBCC@email.com"],
        "subject": "Test subject",
        "text": "Test text",
    }

    # When
    result = EmailSchema(**data)

    # Then
    assert result.to_emails == set(to_email.lower() for to_email in data["to_emails"])
    assert result.cc_emails == set(cc_email.lower() for cc_email in data["cc_emails"])
    assert result.bcc_emails == set(
        bcc_email.lower() for bcc_email in data["bcc_emails"]
    )


def test_check_error_when_both_html_text_not_provided():
    # Given
    data = {
        "from_email": "from@email.com",
        "to_emails": ["to@email.com"],
        "subject": "Test subject",
    }

    # When
    with pytest.raises(ValidationError) as e:
        EmailSchema(**data)

    # Then
    errors = e.value.errors()
    assert len(errors) == 1
    assert errors[0] == {
        "type": "value_error",
        "loc": ("__root__",),
        "msg": 'At least one of: ["html", "text"] fields has to be provided',
    }


@pytest.mark.parametrize(
    "params",
    [
        {"to_emails": ["duplicated@email.com"], "cc_emails": ["duplicated@email.com"]},
        {"to_emails": ["duplicated@email.com"], "bcc_emails": ["duplicated@email.com"]},
        {
            "to_emails": ["notduplicated@email.com"],
            "cc_emails": ["duplicated@email.com"],
            "bcc_emails": ["duplicated@email.com"],
        },
    ],
)
def test_check_no_duplicates_between_recipient_addresses(params):
    # Given
    data = {
        "from_email": "from@email.com",
        "subject": "Test subject",
        "text": "Test text",
        **params,
    }

    # When
    with pytest.raises(ValidationError) as e:
        EmailSchema(**data)

    # Then
    # TODO: Better assert
    errors = e.value.errors()
    assert len(errors) == 1
    assert errors[0] == {
        "type": "value_error",
        "loc": ("__root__",),
        "msg": 'Emails should be unique between: ["to_emails", "cc_emails", "bcc_emails"]',
    }
