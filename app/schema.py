from typing import Optional, Set

from pydantic import BaseModel, EmailStr, conset, root_validator, validator


class EmailSchema(BaseModel):
    from_email: EmailStr
    to_emails: conset(EmailStr, min_items=1)
    cc_emails: Optional[Set[EmailStr]] = None
    bcc_emails: Optional[Set[EmailStr]] = None
    subject: str
    html: Optional[str] = None
    text: Optional[str] = None

    @validator("to_emails", "cc_emails", "bcc_emails", pre=True, each_item=True)
    def lowercase_recipient_emails(cls, v):
        """
        The reasoning behind conversion to lowercase is the fact that
        some email providers do not respect RFC 5321 and case-sensivity of email local part.
        One of such providers is SendGrid.
        """
        return v.lower()

    @root_validator
    def check_either_html_or_text_provided(cls, values):
        html = values.get("html")
        text = values.get("text")

        if html is None and text is None:
            raise ValueError(
                'At least one of: ["html", "text"] fields has to be provided'
            )
        return values

    @root_validator
    def check_no_duplicates_between_email_addresses(cls, values):
        """
        The reasoning behind this constraint is that some email providers do not allow
        duplication between to, cc and bcc emails.
        One of such providers is SendGrid.
        """
        to_emails = values.get("to_emails")
        cc_emails = values.get("cc_emails")
        bcc_emails = values.get("bcc_emails")

        error_msg = (
            'Emails should be unique between: ["to_emails", "cc_emails", "bcc_emails"]'
        )

        if cc_emails and to_emails.intersection(cc_emails):
            raise ValueError(error_msg)

        if bcc_emails and to_emails.intersection(bcc_emails):
            raise ValueError(error_msg)

        if cc_emails and bcc_emails and cc_emails.intersection(bcc_emails):
            raise ValueError(error_msg)

        return values


class SendEmailResponse(BaseModel):
    # TODO: Add error to response
    result: bool
