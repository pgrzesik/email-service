from typing import Optional

from pydantic import BaseSettings, HttpUrl, RedisDsn


class Settings(BaseSettings):
    SENDGRID_API_KEY: str

    MAILGUN_API_KEY: str
    MAILGUN_API_BASE_URL: HttpUrl
    REDIS_URL: RedisDsn

    DEBUG: bool = False

    SENTRY_DSN: Optional[str] = None


settings = Settings()
