from abc import ABC

from app.schema import EmailSchema


class BaseEmailProvider(ABC):
    def send_email(self, email_params: EmailSchema) -> bool:
        raise NotImplementedError
