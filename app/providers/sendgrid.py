import logging

from app.config import settings
from app.providers.base import BaseEmailProvider
from app.schema import EmailSchema
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail, MailSettings, SandBoxMode

logger = logging.getLogger(__name__)


class SendGridEmailProvider(BaseEmailProvider):

    name = "SENDGRID"

    def __init__(self, api_key, sandbox_mode=False):
        self.client = SendGridAPIClient(api_key)
        self.sandbox_mode = sandbox_mode

    def send_email(self, email_params: EmailSchema) -> bool:
        message = self.construct_email_message(email_params)

        try:
            self.client.send(message)
        except Exception as e:
            # TODO: More granular error handling
            err_body = getattr(e, "body", None)
            logger.exception(
                f"Sending email with provider: {self.name} failed. Error body: {err_body}"
            )
            return False

        return True

    def construct_email_message(self, email_params: EmailSchema) -> Mail:
        message = Mail(
            from_email=email_params.from_email,
            to_emails=list(email_params.to_emails),
            subject=email_params.subject,
            plain_text_content=email_params.text,
            html_content=email_params.html,
        )
        if email_params.bcc_emails:
            message.bcc = list(email_params.bcc_emails)
        if email_params.cc_emails:
            message.cc = list(email_params.cc_emails)

        if self.sandbox_mode:
            mail_settings = MailSettings()
            mail_settings.sandbox_mode = SandBoxMode(self.sandbox_mode)
            message.mail_settings = mail_settings

        return message


def get_sendgrid_email_provider() -> SendGridEmailProvider:
    sandbox_mode = settings.DEBUG == True
    return SendGridEmailProvider(settings.SENDGRID_API_KEY, sandbox_mode)
