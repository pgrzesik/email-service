import logging
from urllib.parse import urljoin

import requests

from app.config import settings
from app.providers.base import BaseEmailProvider
from app.schema import EmailSchema

logger = logging.getLogger(__name__)


class MailgunEmailProvider(BaseEmailProvider):

    name = "MAILGUN"

    def __init__(self, api_key, base_url):
        self.api_key = api_key
        self.base_url = base_url
        self.messages_url = urljoin(base_url, "messages")

    def send_email(self, email_params: EmailSchema) -> bool:
        message = self.construct_email_message(email_params)

        try:
            response = requests.post(
                self.messages_url, auth=("api", self.api_key), data=message,
            )
            response.raise_for_status()
        except requests.exceptions.HTTPError as e:
            # TODO: More granular exception handling
            err_response_text = getattr(e.response, "text", None)
            logger.exception(
                f"Sending email with provider: {self.name} failed. Error response text: {err_response_text}"
            )
            return False

        return True

    def construct_email_message(self, email_params: EmailSchema) -> dict:
        message = {
            "from": email_params.from_email,
            "to": list(email_params.to_emails),
            "subject": email_params.subject,
            "text": email_params.text,
            "html": email_params.html,
        }

        if email_params.cc_emails:
            message["cc"] = list(email_params.cc_emails)
        if email_params.bcc_emails:
            message["bcc"] = list(email_params.bcc_emails)

        return message


def get_mailgun_email_provider() -> MailgunEmailProvider:
    return MailgunEmailProvider(settings.MAILGUN_API_KEY, settings.MAILGUN_API_BASE_URL)
