from app.auth import validate_api_key
from app.schema import EmailSchema, SendEmailResponse
from app.services import EmailService, get_email_service
from fastapi import APIRouter, Depends

api_router = APIRouter()


# TODO: TESTS
@api_router.post(
    "/send_email",
    dependencies=[Depends(validate_api_key)],
    response_model=SendEmailResponse,
    summary="Send an email",
)
def send_email(
    email_params: EmailSchema, email_service: EmailService = Depends(get_email_service)
):
    """
    Send an email based on provided parameters:

    - **from_email**: required
    - **to_emails**: a set of unique, case insensitive "to" recipients, required at least 1 entry
    - **cc_emails**: a set of unique, case insensitive "cc" recipients, optional
    - **bcc_emails**: a set of unique, case insensitive "cc" recipients, optional
    - **subject**: required
    - **html**: html content of an email, required if "text" not provided
    - **text**: text content of an email, required if "html" not provided

    Additional notes:
    - Duplicated recipients between "to_emails", "cc_emails", "bcc_emails" are NOT allowed.
    """
    result = email_service.send_email(email_params)

    return SendEmailResponse(result=result)
